﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoqSampleAppWithCelloLibrary.SampleSetup
{
    public interface IServiceContainer
    {
        T Resolve<T>(string registrationName = null);
        void RegisterInstance<T>(T implementation, string regName);
    }

    public class ServiceContainer : IServiceContainer
    {
        IUnityContainer _container;

        public ServiceContainer()
        {
            _container = new UnityContainer();
            UnityConfigurationSection ucs = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            ucs.Containers["services"].Configure(_container);
        }

        public T Resolve<T>(string registrationName = null)
        {
            return _container.Resolve<T>(typeof(T).Name);
        }


        public void RegisterInstance<T>(T implementation, string regName)
        {
            _container.RegisterInstance<T>(regName, implementation);
        }
    }

    public interface IDALContainer
    {
        T Resolve<T>(string registrationName = null);
        void RegisterInstance<T>(T implementation, string regName);
    }

    public class DALContainer : IDALContainer
    {
        IUnityContainer _container;
        public DALContainer()
        {
            _container = new UnityContainer();
            UnityConfigurationSection ucs = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            ucs.Containers["dal"].Configure(_container);
        }

        public T Resolve<T>(string registrationName = null)
        {
            return _container.Resolve<T>(typeof(T).Name);
        }

        public void RegisterInstance<T>(T implementation, string regName)
        {
            _container.RegisterInstance<T>(regName, implementation);
        }
    }

    public static class ServiceLocator
    {
        private const string ContainerName = "services";
        private static IServiceContainer DefaultServiceContainer
        {
            get;
            set;
        }

        static ServiceLocator()
        {
            IUnityContainer unityContainer = new UnityContainer();
            UnityConfigurationSection unityConfigurationSection = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            unityConfigurationSection.Containers[ContainerName].Configure(unityContainer);
            System.Type typeFromHandle = typeof(IServiceContainer);
            if (unityConfigurationSection.Containers[ContainerName] != null)
            {
                DefaultServiceContainer = unityContainer.Resolve<IServiceContainer>("IServiceContainer");
            }
            else
            {
                DefaultServiceContainer = new SingletonServiceContainer();
            }
        }

        public static T Resolve<T>() where T : class
        {
            return ServiceLocator.DefaultServiceContainer.Resolve<T>(null);
        }

        public static void RegisterInstance<TInterface>(TInterface instance, string name)
        {
            DefaultServiceContainer.RegisterInstance<TInterface>(instance, name);
        }
    }
}
