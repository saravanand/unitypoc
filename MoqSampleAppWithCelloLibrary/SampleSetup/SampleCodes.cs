﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoqSampleAppWithCelloLibrary.SampleSetup
{
    public class User
    {
        public string UserName { get; set; }
    }

    public interface IServiceBase { }

    public interface IDALBase { }

    public class BaseService
    {
        public IDALBase dalBase { get; set; }
        [Dependency("IDALContainer")]
        public IDALContainer _dalContainer { get; set; }
    }

    public interface IUserService
    {
        User GetUser(string userName);
    }

    public class UserService : BaseService, IUserService
    {
        public User GetUser(string userName)
        {
            return this._dalContainer.Resolve<IUserDAL>().GetUser(userName);
        }
    }

    public interface IUserDAL
    {
        User GetUser(string userName);
    }

    public class UserDAL : IUserDAL, IDALBase
    {
        public User GetUser(string userName)
        {
            return new User() { UserName = userName };
        }
    }
}
