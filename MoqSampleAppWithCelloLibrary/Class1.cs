﻿using Moq;
using MoqSampleAppWithCelloLibrary.SampleSetup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoqSampleAppWithCelloLibrary
{
    public class Class1
    {
        public static void Main(string[] args)
        {
            //UnityCaller();
            MockedCaller();
            Console.ReadKey();
        }

        private static void UnityCaller()
        {
            string username = "bill";

            IUserService service = new ServiceContainer().Resolve<IUserService>();
            var user = service.GetUser(username);

            if (user != null)
            {
                Console.WriteLine(user.UserName);
            }

        }

        public static void MockedCaller()
        {
            // Set up the DALContainer which is instance based
            var mockDalContainer = new DALContainer();

            Mock<IUserDAL> mockedUserDAL = new Mock<IUserDAL>();

            mockedUserDAL.Setup(m => m.GetUser(It.IsAny<string>())).Returns(new User() { UserName = "saravanan" });

            // Register the mocked DAL Implementation in the DAL Container
            mockDalContainer.RegisterInstance<IUserDAL>(mockedUserDAL.Object, "IUserDAL");
            // Map this instance of DAL against the default implementation of the DAL Container so that in the Service Layer, the mocked DAL is returned inplace of the SQLDAL
            ServiceLocator.RegisterInstance<IDALContainer>(mockDalContainer, "IDALContainer");

            var user = ServiceLocator.Resolve<IUserService>().GetUser("saran");

            if (user != null)
            {
                Console.WriteLine(user.UserName);
            }
        }
    }
}
